
'use strict';
const path = require('path');
const AWS = require('aws-sdk');
const vandium = require('vandium');
let AmazonCognitoIdentity = require('amazon-cognito-identity-js');
AWS.config.update({
    region: 'us-east-1'
});
const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
global.fetch = require('node-fetch')
const POOL = 'us-east-1_jzP884qXc';
const CLIENT = '7o81b1q9tdnug3uqvuf664lekd';

function confirm_login(email_add, pass) {

    let email = email_add,
        password = pass;

    let authenticationData = {
        Username: email,
        Password: password,
    };

    let authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails(authenticationData);
    let poolData = {
        UserPoolId: POOL,
        ClientId: CLIENT
    };

    let userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);
    let userData = {
        Username: email,
        Pool: userPool
    };

    return new Promise((resolve, reject) => {
        let cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);

        cognitoUser.authenticateUser(authenticationDetails, {
            onSuccess: function (result) {
                JSON.stringify(result);
                var accesstoken = result.getAccessToken().getJwtToken();
                console.log(accesstoken)
                resolve(accesstoken);
                // User authentication was successful
            },
            onFailure: function (err) {
                console.log(err)
                resolve(true);
                // User authentication was not successful
            }
        });
    });
}
exports.handler = vandium.api()
  .cors({
    allowOrigin: '*'
  })
  .GET({
    queryStringParameters: {
     email: vandium.types.string().required(),
     password: vandium.types.string().required()
    }
  }, async(event, context) => {

    let email = event.queryStringParameters.email,
    password = event.queryStringParameters.password;

    let access_token = await confirm_login(email,password);
    return access_token;
  })
