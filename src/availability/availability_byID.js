/**
 * Implements a lambda function to implements:
 * GET:
 *  Returns details of car of given car ID
 *
 * @author Ibtisam Tahir
 *
 * Created at: Sat Feb  15 12:56:26 PKT 2020
 */

'use strict';


const path = require('path');
const db_utils = require(path.resolve('./') + '/utils/db_utils.js');
const vandium = require('vandium');

exports.handler = vandium.api()
  .cors({
    allowOrigin: '*'
  })
  .PUT({
    pathParameters: {
      id: vandium.types.number().integer().positive().required(),
    },
    body: {
      availability: vandium.types.string().required()
    }
  }, async(event, context) => {
    let availability_id = event.pathParameters.id,
        availability = event.body.availability;

    const client = db_utils.get_db_client();
    const query = {
      name: 'updateAvailability-availability',
      text: 'UPDATE availability SET availability_date=$1, updated_at=NOW() WHERE id= $2',
      values: [availability, availability_id
      ]
    };
    return client.query(query)
    .then(function () {
      client.end();
      return {
        message: "Availability updated successfully."
      };
    })
  })


  .onError((err, ctx) => {
    return (err);
  });
