/**
 * Implements a lambda function to implements:
 * GET:
 *  Returns availability of cars by car ID
 * POST:
 *  Adds a new availability slot against car
 *
 * @author Ibtisam Tahir
 *
 * Created at: Sat Feb  15 12:56:26 PKT 2020
 */

'use strict';

const vandium = require('vandium');
const path = require('path');
const db_utils = require(path.resolve('./') + '/utils/db_utils.js');


exports.handler = vandium.api()
  .cors({
    allowOrigin: '*'
  })
  .GET({
    queryStringParameters: {
      car_id: vandium.types.number().required(),
    }
  }, async(event, context) => {

    let car_id = event.queryStringParameters.car_id;

    const client = db_utils.get_db_client();
    
    let res = await getCarAvailability(client,car_id);
    return res;
  })
  .POST({
    // validate
    body: {
      availability: vandium.types.string().required(),
      car_id: vandium.types.number().required()
    }
  }, async(event, context) => {
    // Get DB client to query
    const client = db_utils.get_db_client();

    // Initialize variables
    let availability = event.body.availability,
    car_id = event.body.car_id;
    
    let res = await addCarAvailability(client,availability,car_id);
      return res;
  })
  .onError((err, ctx) => {
    return (err);
  });

  async function addCarAvailability(client,availability,car_id){
    const query = {
      name: "insertAvailability-availability",
      text:
        "INSERT INTO availability (availability_date, car_id)\
        VALUES($1, $2)",
      values: [
        availability,
        car_id
      ]
    };

    return client
      .query(query)
      .then(function (result) {
        client.end();
        
        return {
          message: "Car availability added successfully."
        };

      })
  }

  async function getCarAvailability(client,car_id){
let SQL_TEXT = `select availability_date, car_id
from availability
where car_id = $1`
let params = [];
params.push(car_id);

return client.query(SQL_TEXT)
            .then((res) => {
              client.end();
              if (res.rows != null && res.rows.length > 0) {
                return res.rows;
              }
              else{
                return [];
              }

            })

}