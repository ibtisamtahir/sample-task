/**
 * Implements a lambda function to implements:
 * GET:
 *  Returns details of cars filterable by date/availability
 * POST:
 *  Adds a new car with given details
 *
 * @author Ibtisam Tahir
 *
 * Created at: Sat Feb  15 12:56:26 PKT 2020
 */

'use strict';

const vandium = require('vandium');
const path = require('path');
const db_utils = require(path.resolve('./') + '/utils/db_utils.js');


exports.handler = vandium.api()
  .cors({
    allowOrigin: '*'
  })
  .GET({
    queryStringParameters: {
      pagination_limit: vandium.types.number().integer().min(1).default(25),
      pagination_offset: vandium.types.number().integer().min(0).default(0),
      order_by: vandium.types.string().regex(/^(created_at|updated_at|car_availability_date)$/).default('created_at'),
      order_asc: vandium.types.boolean().default(true)
    }
  }, async(event, context) => {

    let pagination_offset = event.queryStringParameters.pagination_offset,
      pagination_limit = event.queryStringParameters.pagination_limit,
      order_by = event.queryStringParameters.order_by,
      order_asc = event.queryStringParameters.order_asc;

    const client = db_utils.get_db_client();
    
    let res = await getCar(client,pagination_offset,pagination_limit,order_by,order_asc);
    return res;
  })
  .POST({
    // validate
    body: {
      model: vandium.types.string().required(),
      make: vandium.types.string().required(),
      manufacturing_year: vandium.types.string().required(),
      features: vandium.types.string().required(),
      user_id: vandium.types.number().required(),
      car_image: vandium.types
        .string({ trim: false })
        .base64({ paddingRequired: false }),
      price: vandium.types.number().integer().positive(),
    }
  }, async(event, context) => {
    // Get DB client to query
    const client = db_utils.get_db_client();

    // Initialize variables
    let model = event.body.model,
    make = event.body.make,
    manufacturing_year = event.body.manufacturing_year,
    features = event.body.features,
    price = event.body.price,
    user_id = event.body.user_id,
    car_image = event.body.car_image;
    
    let res = await addCar(client,model,make,manufacturing_year,features,price,user_id,car_image);
      return res;
  })
  .onError((err, ctx) => {
    return err;
  });

  async function addCar(client,model,make,manufacturing_year,features,price,user_id,car_image){
    const query = {
      name: "insertCar-car",
      text:
        "INSERT INTO cars \
        (model, make, manufacturing_year, price, user_id, features) \
        VALUES($1, $2, $3, $4, $5, $6)",
      values: [
        model,
      make,
      manufacturing_year,
      price,
      user_id,
      features
      ]
    };

    return client
      .query(query)
      .then(function (result) {
        client.end();
        return {
          message: "Car added successfully."
        };

      })

  }

  async function getCar(client,pg_offset,pg_limit,order_by,order_asc){
 let SQL_TEXT = `select model,make,manufacturing_year,features,price,created_at,updated_at,
 (case when car_availability_date is null then null else car_availability_date end)
 from cars
 left join (select min(availability_date) as car_availability_date, car_id
 from availability group by car_id )  car_availability_date
 on car_availability_date.car_id = cars. car_id
 `
 if (order_by !== undefined) {
  SQL_TEXT += " ORDER BY " + order_by.toLowerCase();
}

SQL_TEXT = SQL_TEXT.concat(order_asc ? " ASC " : " DESC ");

SQL_TEXT += " LIMIT ";
SQL_TEXT = SQL_TEXT.concat(pg_limit ? parseInt(pg_limit) : 'ALL');

SQL_TEXT += "  OFFSET ";
SQL_TEXT = SQL_TEXT.concat(pg_offset ? parseInt(pg_offset) : '0');

return client.query(SQL_TEXT)
            .then((res) => {
              client.end();
              if (res.rows != null && res.rows.length > 0) {
                return res.rows;
              }
              else{
                return [];
              }

            })


}