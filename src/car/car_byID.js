/**
 * Implements a lambda function to implements:
 * GET:
 *  Returns details of car of given car ID
 *
 * @author Ibtisam Tahir
 *
 * Created at: Sat Feb  15 12:56:26 PKT 2020
 */

'use strict';


const path = require('path');
const db_utils = require(path.resolve('./') + '/utils/db_utils.js');
const vandium = require('vandium');

exports.handler = vandium.api()
  .cors({
    allowOrigin: '*'
  })
  .GET({
    pathParameters: {
      id: vandium.types.number().integer().positive().required(),
    }
  }, async(event, context) => {
    let car_id = event.pathParameters.id;
    const client = db_utils.get_db_client();
    let res = await getCarDetails(client,car_id);
    return res;
  }).onError((err, ctx) => {
    return (err);
  });


  async function getCarDetails(client,id){
    let SQL_TEXT = `select model,make,manufacturing_year,features,price,
    (case when car_availability_date is null then null else car_availability_date end)
    from cars
    left join (select array_agg(availability_date) as car_availability_date, car_id
    from availability group by car_id )  car_availability_date
    on car_availability_date.car_id = cars. car_id
    where cars.car_id = $1    
    `
    let params = [];
    params.push(id);
    
   
   return client.query(SQL_TEXT,params)
               .then((res) => {
                 client.end();
                 if (res.rows != null && res.rows.length > 0) {
                   return res.rows;
                 }
                 else{
                   return [];
                 }
   
               })
   
   
   }